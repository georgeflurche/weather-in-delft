from selenium import webdriver
from bs4 import BeautifulSoup
import re
import sys
import logging
import time


logging.basicConfig(
    format='%(levelname)s [%(module)s]: %(message)s'
)
_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)

SOURCE_INFO = "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php"


def get_temperature():
    op = webdriver.ChromeOptions()
    op.add_argument('--headless')
    op.add_argument("--no-sandbox")
    op.add_argument("--disable-dev-shm-usage")
    op.add_experimental_option('excludeSwitches', ['enable-logging'])

    driver = webdriver.Chrome(options=op)
    driver.get(SOURCE_INFO)

    # Wait for the updates in html page
    time.sleep(1)
    html_source = driver.page_source
    driver.quit()

    _logger.debug("The html source code is:\n"+html_source)

    soup = BeautifulSoup(html_source, 'lxml')
    thermometer = soup.find('span', class_="ajax", id="ajaxthermometer")
    _logger.debug("The thermometer data is:\n"+str(thermometer))

    last_obs = thermometer.attrs.get('lastobs', '')
    _logger.debug("The last observation on the site is: "+last_obs)

    regex_pattern = r'(?<=Currently: )-?\d+(\.\d+)?(?=&)'
    try:
        re.compile(regex_pattern)
    except Exception as e:
        _logger.error(
            'Failed to compile the pattern: '+regex_pattern+"\n"+str(e))
        sys.exit(1)
    search_obj = re.search(regex_pattern, last_obs)
    if search_obj is not None:
        temp = search_obj.group(0)
    else:
        _logger.info("No match found in the string: '"+last_obs+"'")
        sys.exit(1)
    rounded_T = round(float(temp))
    return rounded_T


if __name__ == "__main__":
    rounded_T = get_temperature()
    print(str(rounded_T)+" degrees Celsius")
